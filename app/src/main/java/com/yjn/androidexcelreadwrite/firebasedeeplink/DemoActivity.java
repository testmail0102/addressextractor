package com.yjn.androidexcelreadwrite.firebasedeeplink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.yjn.androidexcelreadwrite.R;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.BranchContentSchema;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.CurrencyType;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ProductCategory;
import io.branch.referral.util.ShareSheetStyle;

public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);


        Button share=findViewById(R.id.share);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }

                        System.out.println("deep link--"+deepLink);
                        if(deepLink!=null) {
                            Intent i = new Intent(DemoActivity.this, Demo1Activity.class);
                            startActivity(i);
                        }
                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Myapp", "getDynamicLink:onFailure", e);
                    }
                });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri newstr=Uri.parse("https://javatpoint.com");
                shareDeepLink(buildDeepLink(newstr).toString());
//                    LinkProperties linkProperties = new LinkProperties()
//                            .addTag("myShareTag1")
//                            .addTag("myShareTag2")
////                        .setAlias("mylinkName") // In case you need to white label your link
//                            .setChannel("myShareChannel2")
//                            .setFeature("mySharefeature2")
//                            .setStage("10")
//                            .setCampaign("Android campaign")
////                        .addControlParameter("$android_deeplink_path", "custom/path/*")
////                        .addControlParameter("$ios_url", "http://example.com/ios")
//                            .addControlParameter("$ios_deepview", "default_template")
//                            .addControlParameter("$android_deepview", "default_template")
//                            .setDuration(100);
//
//                    //noinspection deprecation
//                    ShareSheetStyle shareSheetStyle = new ShareSheetStyle(DemoActivity.this, "My Sharing Message Title", "My Sharing message body")
//                            .setCopyUrlStyle(getResources().getDrawable(android.R.drawable.ic_menu_send), "Save this URl", "Link added to clipboard")
//                            .setMoreOptionStyle(getResources().getDrawable(android.R.drawable.ic_menu_search), "Show more")
//                            .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
//                            .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
//                            .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
//                            .addPreferredSharingOption(SharingHelper.SHARE_WITH.TWITTER)
//                            .setAsFullWidthStyle(true)
//                            .setSharingTitle("Share With");
//                    // Define custom style for the share sheet list view
//                    //.setStyleResourceID(R.style.Share_Sheet_Style);
//// Create a BranchUniversal object for the content referred on this activity instance
//                BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
//                        .setCanonicalIdentifier("item/12345")
//                        .setCanonicalUrl("https://branch.io/deepviews")
//                        .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PRIVATE)
//                        .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
//                        .setTitle("My Content Title")
//                        .setContentDescription("my_product_description1")
//                        .setContentImageUrl("https://example.com/mycontent-12345.png")
//                        .setContentExpiration(new Date(1573415635000L))
//                        .setContentImageUrl("https://test_img_url")
//                        .addKeyWord("My_Keyword1")
//                        .addKeyWord("My_Keyword2")
//                        .setContentMetadata(
//                                new ContentMetadata().setProductName("my_product_name1")
//                                        .setProductBrand("my_prod_Brand1")
//                                        .setProductVariant("3T")
//                                        .setProductCategory(ProductCategory.BABY_AND_TODDLER)
//                                        .setProductCondition(ContentMetadata.CONDITION.EXCELLENT)
//                                        .setAddress("Street_name1", "city1", "Region1", "Country1", "postal_code")
//                                        .setLocation(12.07, -97.5)
//                                        .setSku("1994320302")
//                                        .setRating(6.0, 5.0, 7.0, 5)
//                                        .addImageCaptions("my_img_caption1", "my_img_caption_2")
//                                        .setQuantity(2.0)
//                                        .setPrice(23.2, CurrencyType.USD)
//                                        .setContentSchema(BranchContentSchema.COMMERCE_PRODUCT)
//                                        .addCustomMetadata("Custom_Content_metadata_key1", "Custom_Content_metadata_val1")
//                        );
//
//                    branchUniversalObject.showShareSheet(DemoActivity.this, linkProperties, shareSheetStyle, new Branch.BranchLinkShareListener() {
//
//                                @Override
//                                public void onShareLinkDialogLaunched() {
//                                }
//
//                                @Override
//                                public void onShareLinkDialogDismissed() {
//                                }
//
//                                @Override
//                                public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
//                                    System.out.println("sharedlink---------"+sharedLink);
//                                }
//
//
//                                @Override
//                                public void onChannelSelected(String channelName) {
//                                }
//
//                                /*
//                                 * Use {@link io.branch.referral.Branch.ExtendedBranchLinkShareListener} if the params need to be modified according to the channel selected by the user.
//                                 * This allows modification of content or link properties through callback {@link #onChannelSelected(String, BranchUniversalObject, LinkProperties)} }
//                                 */
////                            @Override
////                            public boolean onChannelSelected(String channelName, BranchUniversalObject buo, LinkProperties linkProperties) {
////                                linkProperties.setAlias("http://bnc.lt/alias_link");
////                                buo.setTitle("Custom Title for selected channel : " + channelName);
////                                return true;
////                            }
//
//                            },
//                            new Branch.IChannelProperties() {
//                                @Override
//                                public String getSharingTitleForChannel(String channel) {
//                                    return channel.contains("Messaging") ? "title for SMS" :
//                                            channel.contains("Slack") ? "title for slack" :
//                                                    channel.contains("Gmail") ? "title for gmail" : null;
//                                }
//
//                                @Override
//                                public String getSharingMessageForChannel(String channel) {
//                                    return channel.contains("Messaging") ? "message for SMS" :
//                                            channel.contains("Slack") ? "message for slack" :
//                                                    channel.contains("Gmail") ? "message for gmail" : null;
//                                }
//                            });


            }
        });
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Branch branch = Branch.getInstance();
//// Branch init
//        branch.initSession(new Branch.BranchReferralInitListener() {
//            @Override
//            public void onInitFinished(JSONObject referringParams, BranchError error) {
//                if (error == null) {
//                    // params are the deep linked params associated with the link      that the user clicked -> was re-directed to this app
//                    // params will be empty if no data found
//                    // … insert custom logic here …
//                    Log.i("BRANCH SDK", referringParams.toString());
//                } else {
//                    Log.i("BRANCH SDK", error.getMessage());
//                }
//            }
//        }, this.getIntent().getData(), this);
//    }
//    @SuppressLint("MissingSuperCall")
//    @Override
//    public void onNewIntent(Intent intent) {
//        this.setIntent(intent);
//    }

    public Uri buildDeepLink(Uri deepLink) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(deepLink.toString()))
                .setDomainUriPrefix("https://dahiya.page.link")
                // Open links with this app on Android
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                // Open links with com.example.ios on iOS
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.example.ios").build())
                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();

        return dynamicLinkUri;
    }

    void shareDeepLink(String deepLink) {
        Intent intent=  new  Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);
        startActivity(intent);
    }
}
