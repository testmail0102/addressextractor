package com.yjn.androidexcelreadwrite.realmdemo;

import android.app.Application;

import io.branch.referral.Branch;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {
 
    @Override
    public void onCreate() {
 
        super.onCreate();
        // Initialize the Branch object
        Branch.getAutoInstance(this);
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
 
    }
}