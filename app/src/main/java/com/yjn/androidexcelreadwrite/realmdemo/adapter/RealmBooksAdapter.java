package com.yjn.androidexcelreadwrite.realmdemo.adapter;

import android.content.Context;

import com.yjn.androidexcelreadwrite.realmdemo.model.Book;

import io.realm.RealmResults;

public class RealmBooksAdapter extends RealmModelAdapter<Book> {
 
    public RealmBooksAdapter(Context context, RealmResults<Book> realmResults, boolean automaticUpdate) {
 
        super(context, realmResults, automaticUpdate);
    }
}