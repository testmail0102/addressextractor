package com.yjn.androidexcelreadwrite.branchio;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.yjn.androidexcelreadwrite.R;
import com.yjn.androidexcelreadwrite.firebasedeeplink.Demo1Activity;

import org.json.JSONObject;

import java.util.Date;
import java.util.UUID;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;

public class DeepLinkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);


        Button share=findViewById(R.id.share);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentMetadata contentMetadata = new ContentMetadata();
                contentMetadata.addCustomMetadata("content_id", "potato");

                BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                        .setCanonicalIdentifier(UUID.randomUUID().toString())
                        .setTitle("Content Title!")
                        .setContentDescription("Check out this Content!")
                        .setContentMetadata(contentMetadata) //content id goes here
                        .setContentImageUrl("http://i.imgur.com/z6jhNiM.jpg") //a thumbnail preview for related image goes here
                        .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC);

                LinkProperties linkProperties = new LinkProperties()
                        .addControlParameter("$desktop_url", "https://f22labs.com")
                        .addControlParameter("$ios_url", "https://f22labs.com");
                showShareView(linkProperties, branchUniversalObject);

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance();
// Branch init
        branch.initSession((branchUniversalObject, linkProperties, error) ->
        {
            if (error == null) {
                if (!branchUniversalObject.getContentMetadata().getCustomMetadata().isEmpty()
                        && branchUniversalObject.getContentMetadata().getCustomMetadata().containsKey("content_id")) {
                    String contentId = branchUniversalObject.getContentMetadata().getCustomMetadata().get("content_id");
                    Toast.makeText(this, "Deep Link Content ID: " + contentId, Toast.LENGTH_SHORT).show();
                }
            } else {
                Log.i("TAG", error.getMessage());
            }
        },getIntent().getData(),this);
    }

    @SuppressLint("MissingSuperCall")
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    private void showShareView(LinkProperties linkProperties,BranchUniversalObject branchUniversalObject) {

        branchUniversalObject.generateShortUrl(this,linkProperties,new Branch.BranchLinkCreateListener()
        {

            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.v("TAG", "\"got my Branch link to share: " + url);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(Intent.createChooser(intent, "Share trailer!"));
                } else {
                    Log.e("TAG", "onLinkCreate: Branch error: " + error.getMessage());
                }
            }
        });

    }

}
