package com.yjn.androidexcelreadwrite.exceldatareadwrite.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppConfig {

   public static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

   public static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.MINUTES)
            .readTimeout(20, TimeUnit.MINUTES)
            .writeTimeout(20, TimeUnit.MINUTES)
           .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();
    public static String BASE_URL = "http://10.11.8.91:919/";
    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()  
                .baseUrl(AppConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();  
    }  
}  