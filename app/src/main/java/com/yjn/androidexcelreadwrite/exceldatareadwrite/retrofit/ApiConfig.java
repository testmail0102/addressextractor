package com.yjn.androidexcelreadwrite.exceldatareadwrite.retrofit;

import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiConfig {
    @Multipart
    @POST("Unit/UploadRailwayPassengersAPI")
    Call<FileRes> uploadFile(@Part("ArrivalDate") RequestBody date, @Part MultipartBody.Part file);

}  