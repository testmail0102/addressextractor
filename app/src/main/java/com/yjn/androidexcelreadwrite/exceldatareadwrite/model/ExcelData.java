package com.yjn.androidexcelreadwrite.exceldatareadwrite.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ExcelData extends RealmObject {

    @PrimaryKey
    private int sl_no;
 
    private String jrny_date;
 
    private int trno;
 
    private String boardingpoint;
 
    private String psg_name;
    int age;

    String gender;
    private String mobno;
    private String destination_address;
    private String latitude;
    private String longitude;

    public int savedindx=0;


    public int getSavedindx() {
        return savedindx;
    }

    public void setSavedindx(int savedindx) {
        this.savedindx = savedindx;
    }

    public int getSl_no() {
        return sl_no;
    }

    public void setSl_no(int sl_no) {
        this.sl_no = sl_no;
    }

    public String getJrny_date() {
        return jrny_date;
    }

    public void setJrny_date(String jrny_date) {
        this.jrny_date = jrny_date;
    }

    public int getTrno() {
        return trno;
    }

    public void setTrno(int trno) {
        this.trno = trno;
    }

    public String getBoardingpoint() {
        return boardingpoint;
    }

    public void setBoardingpoint(String boardingpoint) {
        this.boardingpoint = boardingpoint;
    }

    public String getPsg_name() {
        return psg_name;
    }

    public void setPsg_name(String psg_name) {
        this.psg_name = psg_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getDestination_address() {
        return destination_address;
    }

    public void setDestination_address(String destination_address) {
        this.destination_address = destination_address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}