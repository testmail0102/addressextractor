package com.yjn.androidexcelreadwrite.exceldatareadwrite;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;
import java.util.Map;

public class BackgroundNotificationService extends IntentService {

    public BackgroundNotificationService() {
        super("Service");
    }

    public static NotificationCompat.Builder notificationBuilder;
    public static  NotificationManager notificationManager;
    ExcelUtil excelUtil;
    public  static Context context;

    @Override
    protected void onHandleIntent(Intent intent) {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        context=this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("id", "an", NotificationManager.IMPORTANCE_LOW);

            notificationChannel.setDescription("no sound");
            notificationChannel.setSound(null, null);
            notificationChannel.enableLights(false);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(false);
            notificationManager.createNotificationChannel(notificationChannel);

        }

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setAction("android.intent.action.MAIN");
        resultIntent.addCategory("android.intent.category.LAUNCHER");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder = new NotificationCompat.Builder(this, "id")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentTitle("Importing excel")
                .setContentText("")
                .setDefaults(0)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        Notification notification=notificationBuilder.build();
        //notificationManager.notify(0, notification);
        startForeground(0,notification);
       // Notification notification=notificationBuilder.build();
       // startForeground(1, notificationBuilder.build());

     //   initRetrofit();

    }
//
//    private void initRetrofit() {
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://unsplash.com/")
//                .build();
//
//        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
//
//        Call<ResponseBody> request = retrofitInterface.downloadImage("photos/YYW9shdLIwo/download?force=true");
//        try {
//
//            downloadImage(request.execute().body());
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//
//        }
//    }
//
//    private void downloadImage(ResponseBody body) throws IOException {
//
//        int count;
//        byte data[] = new byte[1024 * 4];
//        long fileSize = body.contentLength();
//        InputStream inputStream = new BufferedInputStream(body.byteStream(), 1024 * 8);
//        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "journaldev-image-downloaded.jpg");
//        OutputStream outputStream = new FileOutputStream(outputFile);
//        long total = 0;
//        boolean downloadComplete = false;
//        //int totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
//
//        while ((count = inputStream.read(data)) != -1) {
//
//            total += count;
//            int progress = (int) ((double) (total * 100) / (double) fileSize);
//
//
//            updateNotification(progress);
//            outputStream.write(data, 0, count);
//            downloadComplete = true;
//        }
//        onDownloadComplete(downloadComplete);
//        outputStream.flush();
//        outputStream.close();
//        inputStream.close();
//
//    }


    public static void updateNotification(int currentProgress) {


        notificationBuilder.setProgress(100, currentProgress, false);
        notificationBuilder.setContentText(currentProgress + "%");
        notificationManager.notify(0, notificationBuilder.build());
    }


    public static void sendProgressUpdate(boolean downloadComplete) {

        Intent intent = new Intent(MainActivity.PROGRESS_UPDATE);
        intent.putExtra("downloadComplete", downloadComplete);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void onDownloadComplete(boolean downloadComplete) {
        sendProgressUpdate(downloadComplete);
        //NotificationManagerCompat.from(context).cancel(0);
        notificationManager.cancel(0);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done);
        notificationBuilder.setContentTitle("Excel Imported Successfully");
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        NotificationManagerCompat.from(context).cancel(0);
    }

}