package com.yjn.androidexcelreadwrite.exceldatareadwrite.retrofit;

import com.google.gson.annotations.SerializedName;


public class FileRes {

    /**
     * data : 0 Record Added.
     */

    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
